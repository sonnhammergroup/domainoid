
from collections import OrderedDict
from sys import stdout
from Bio import SeqIO
import re

class DomainLoader(object):

    def __init__(self, path, indexed):
        skip = re.compile('^(#|\n).*')
        self.collection = OrderedDict()
        self.repeats = set()
        self.overlap = []
        self.contained = []
        with open(path, 'r') as tsv:
            for line in tsv:
                # Skip comments and empty lines
                if re.match(skip, line):
                    continue

                # Parse fields
                field = line.rstrip().split()

                # Extract fields
                seqid, alnstart, alnend, envstart, envend = field[0:5]
                hmmacc, hmmname, pfam, hmmstart, hmmend, hmmlen = field[5:11]
                bitscore, evalue, significance, clan = field[11:15]
                seqlen = len(indexed[seqid])

                if pfam == 'Repeat':
                    self.repeats.add(hmmacc)

                # Populate collection
                dom = [int(seqlen), int(alnend) - int(alnstart)+1,
                       int(alnstart), int(alnend), hmmacc]
                if seqid not in self.collection:
                    self.collection[seqid] = [dom]
                else:
                    self.collection[seqid].append(dom)

    # Return raw collection
    def getCollection(self):
        return(self.collection)

    def getRepeats(self):
        return(self.repeats)

    def getOverlap(self):
        return(self.overlap)
    def getContained(self):
        return(self.contained)

    # Returns sequentially non-overlapping domains
    # keeping the largest domain in case of overlap
    # Optional parameter minlen of domain
    def getNonOverlappingCollection(self, minlen=0):
        temp_collection = OrderedDict()
        for seqid, domains in self.collection.items():
            # Sort domains by alnstart
            domains = iter(
                sorted(domains, key=lambda x: int(x[2]), reverse=False))
            # Loop domains to find overlaps
            prev = next(domains)
            temp = [prev]
            for domain in domains:
                # If domain overlapping
                if prev[2] <= domain[2] and prev[3] >= domain[3]:
                    self.contained += [domain]
                    # If domain is completely inside, dont add it
                    continue
                elif prev[3] > domain[2] and prev[3] < domain[3]:
                    self.overlap += [domain]
                    # If domain is partly overlapping
                    prevOverlap=(prev[3] - domain[2])/prev[1]
                    domOverlap=(prev[3] - domain[2])/domain[1]
                    if domOverlap < 0.5:
                        if prevOverlap > 0.5:
                            # removing previous domain if overlap is larger than 50%
                            if len(temp) > 0:
                                temp.pop()
                        # Adding domain if overlap is smaller than 50%
                        temp.append(domain)
                        prev = domain
                    elif domOverlap > 0.5:
                        if prevOverlap > 0.5:
                        # keeping the longest if both overlaps more than 50%
                            if prev[1] < domain[1]:
                                if len(temp) > 0:
                                    temp.pop()
                                    temp.append(domain)
                                    prev = domain
                # If domain not overlapping
                else:
                    temp.append(domain)
                    # Update previous domain
                    prev = domain
            # Update new collection
            temp_collection[seqid] = list(
                filter(lambda x: x[1] >= minlen, temp))
        return(temp_collection)


class PseudoDomainLoader(object):

    def __init__(self, collect, minlen=30):
        self.temp_collection = OrderedDict()
        for seqid, domains in collect.items():
            # Get coordinates for domains as tuples
            coordinate = []
            length = 0
            self.temp_collection[seqid] = domains
            for domain in domains:
                start, end = domain[2:4]
                length = domain[0]
                coordinate.append((start, end))
            region_counter = 1
            for region in self.iter_unannotated(coordinate, length):
                start, end = region
                region_length = abs(end - start)+1
                if region_length >= minlen:
                    dom = [length, region_length, start,
                           end, "UNK{:d}".format(region_counter)]
                    self.temp_collection[seqid].append(dom)
                    region_counter += 1


    # Given sorted tuples of coordinates
    # returns unannotated coordinates as tuples
    def iter_unannotated(self, coord, end):
        prev = 0
        for start, stop in coord:
            if prev <= start:
                yield (prev+1, start-1)
            prev = stop
        if prev < end:
            yield (prev+1, end)

    # Return collection of domains
    # including unannotated regions of sufficient length
    def getWithPseudoDomains(self):
        return(self.temp_collection)


class DomainExporter(object):

    def __init__(self, collect, indexed, repeats, overlap, contained):
        self.count = {'WithDomain': 0,
                      'WithoutDomain': 0,
                      'WithDomainRepeat': 0,
                      'WithCompletleyOverlappingDomins': 0,
                      'WithMoreThan50%OverlappingDomins': 0}

        # Filter for non-empty items
        collection = dict(filter(lambda x: len(x[1]) > 0, collect.items()))

        # Get seqid which are not represented by domains
        left = set(indexed.keys()).difference(collection.keys())

        self.output = []
        for index, seqid in enumerate(collection.keys()):
            domains = collection[seqid]
            for domain in domains:
                name = '_'.join([str(x) for x in domain])
                comp_name = seqid + '||' + name
                start, end = domain[2:4]
                chunk = SeqIO.SeqRecord(indexed[seqid].seq[start-1:end],
                                        id=comp_name)
                chunk.description = ''
                # Do not add if domain is a repeat
                if domain[-1] not in repeats:
                    self.output.append(chunk)
                    self.count['WithDomain'] += 1
                elif domain[-1] in repeats:
                    self.count['WithDomainRepeat'] += 1
        for seqid in left:
            length = len(indexed[seqid].seq)
            name = seqid.split(" ")[0]+"||"+ str(length)+"_" +str(length)+"_" + "1"+"_" + str(length) +"_" +"UNK1"
            chunk = SeqIO.SeqRecord(indexed[seqid].seq, id=name)
            chunk.description = ''
            self.output.append(chunk)
            self.count['WithoutDomain'] += 1
        self.count['WithCompletleyOverlappingDomins']=len(contained)
        self.count['WithPartlyOverlappingDomins']=len(overlap)

    def exportTo(self, outpath):
        SeqIO.write(self.output, outpath, 'fasta')
