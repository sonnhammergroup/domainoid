#!/bin/bash
cd /home/domainoid
echo "Running Domainoid from docker container"
python3 runDomainoid.py -f /home/domainoid/input -o /home/domainoid/output -lps /home/pfam/PfamScan -lp /home/pfam/Pfam "$@"
