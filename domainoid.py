from itertools import product, combinations
from sortedcontainers import SortedSet
import networkx as nx
from Bio import SeqIO
import os


class GroupTableReader(object):
    def __init__(self, path):
        self.groups = {}
        with open(path, 'r') as table:
            for line in table:
                values=[]
                lst = line.rstrip().split()
                group, bitscore, basename, score, identifier = lst[0:5]
                species = basename.split('.', 1)[0].replace('_', '')
                name, params = identifier.split('||')
                values += [name]
                values += params.split('_')
                values += [species]
                if group not in self.groups:
                    self.groups[group] = [values]
                else:
                    self.groups[group] += [values]
    def readGroup(self):
        return(self.groups)

class GroupTableReaderNormal(object):
    def __init__(self, path):
        self.groups = {}
        with open(path, 'r') as table:
            for line in table:
                values=[]
                lst = line.rstrip().split()
                group, bitscore, basename, score, identifier = lst[0:5]
                species = basename.split('.', 1)[0].replace('_', '')
                values += [identifier]
                values += [species]
                if group not in self.groups:
                    self.groups[group] = [values]
                else:
                    self.groups[group] += [values]
    def readGroup(self):
        return(self.groups)

class PotentialPairs(object):
    def __init__(self, reader):
        self.potential_pairs = {}
        for igroupid, group in reader.readGroup().items():
            current_pair = {}
            for item in group:
                spname = item[-1]
                idname = item[0]
                if spname not in current_pair:
                    current_pair[spname] = set([idname])
                else:
                    current_pair[spname].add(idname)
            for comb in combinations(current_pair.values(), 2):
                for pair in product(comb[0], comb[1]):
                    if igroupid not in self.potential_pairs:
                        self.potential_pairs[igroupid] = set([pair])
                    else:
                        self.potential_pairs[igroupid].add(pair)
    def getPotentialPair(self):
        return(self.potential_pairs)


class DomainClusters(object):
    def __init__(self, reader):
        self.comap = {}
        for groupid, group in reader.readGroup().items():
            for hit in group:
                protein = hit[0]  # Removed stuff here!!!
                if protein not in self.comap:
                    self.comap[protein] = [groupid]
                else:
                    self.comap[protein].append(groupid)
    def getProteinComap(self):
        return(self.comap)


class CountDomains(object):
    def __init__(self, files):
        self.fastadom = {}
        for filepath in files:
            index = SeqIO.index(filepath, 'fasta')
            for chunk in index.keys():
                sch = chunk.split('||')
                protein = sch[0]
                domain = sch[-1]
                self.fastadom.setdefault(protein, []).append(domain)
    def getMappings(self):
        return(self.fastadom)


class OrthologPairs(object):
    def __init__(self, ppairs, domclu,
                 fastadom, threshold):
        dommap = fastadom.getMappings()
        self.inferred_pairs = SortedSet()
        self.notIncludedPairs = SortedSet()
        notIncludedCount=0
        totalCount=0
        addedCount=0
        for index, pairlist in ppairs.getPotentialPair().items():
            for pair in pairlist:

                A, B = list(pair)
                Alist = domclu.getProteinComap()[A]
                Blist = domclu.getProteinComap()[B]
                Adoms = dommap[A]
                Bdoms = dommap[B]

                if len(Alist) != 0 and len(Blist) != 0:
                    totalCount+=1
                    ratio = 0
                    count = 0
                    count = self.countOccurences(Alist, Blist)
                    total = len(Adoms) + len(Bdoms)
                    ratio = float(count) / float(total)
                    if ratio >= threshold:
                        self.inferred_pairs.add(pair)
                        addedCount+=1


    def countOccurences(self, alist, blist):
        # Repeats are counted only if they appear in both lists
        count = 0
        # Make copies so reference is not affected
        cpyAlist = list(alist)
        cpyBlist = list(blist)
        for group in cpyAlist:
            if group in cpyBlist:
                cpyBlist.remove(group)
                count += 1
        return(count*2)

    def getPairs(self):
        return(self.inferred_pairs)

class Discordant(object):
    def __init__(self, reader, domclu, fastadom, f1, f2, filepath):
        self.discordant=[]
        dommap = fastadom.getMappings() # Names of all domains for prot.
        domainClusters=domclu.getProteinComap() # Numbers for all orth groups for prot
        sqltable= reader.readGroup() # Entire sqlTable
        indexedF1 = SeqIO.index(filepath+f1.split(".dom")[0], format='fasta')
        indexedF2 = SeqIO.index(filepath+f2.split(".dom")[0], format='fasta')

        #  Getting proteins with more than one orthologous domain
        for prot in domainClusters:
            if len(domainClusters[prot])>1:
                discordantGroup = []
                sameGroupProts =set()
                primaryDoms= []

                # Finding Primary species
                primarySpecies=None
                for groupProt in sqltable[domainClusters[prot][0]]:
                    if groupProt[0] == prot:
                        primarySpecies=groupProt[6]

                # Getting all proteins belonging to the same ortholog groups, but is not the same species
                for group in domainClusters[prot]:
                    for groupProt in sqltable[group]:
                        if groupProt[0] != prot and groupProt[6]!=primarySpecies:
                            sameGroupProts.add(groupProt[0])
                        elif groupProt[0] == prot:
                            primaryDoms.append(group +"_"+"|".join(groupProt[1:6]))

                # Needs to have at least two secondary proteins
                if len(sameGroupProts)>1:

                    # And all should not contain the SAME subset
                    intersectSet=set()
                    for groupProt in sameGroupProts:
                        intersect = list(set(domainClusters[prot]).intersection(domainClusters[groupProt]))

                        # And secondaries should not contain all the exact same orthologous groups as the primary
                        containsAll = all(elem in intersect for elem in domainClusters[prot])
                        if containsAll==False:
                            intersectSet.add("_".join(intersect))
                    if len(intersectSet) > 1:

                        # Translating group numbers to domain names + adding remaining, ungrouped domains
                        primaryDoms.extend(self.getUnAssignedDomains(dommap, prot, primaryDoms))
                        discordantGroup.append("\n\nPrimary: "+self.getDescription(primarySpecies, prot, f1, f2, indexedF1, indexedF2, True)+"\n"+", ".join(primaryDoms))
                        for gProt in sameGroupProts:
                            numberedDomains=self.getAssignedDomains(domainClusters, sqltable, gProt)
                            numberedDomains.extend(self.getUnAssignedDomains(dommap, gProt, numberedDomains))
                            discordantGroup.append("Secondary: "+self.getDescription(primarySpecies, gProt, f1, f2, indexedF1, indexedF2, False)+"\n"+", ".join(numberedDomains))
                        self.discordant.append(discordantGroup)

    def getDescription(self, primarySpecies, prot, f1, f2, indexedF1, indexedF2, primary):
        description = None
        if primarySpecies == f1.split(".")[0]:
            if primary==True:
                description = indexedF1[prot].description
            else:
                description = indexedF2[prot].description
        elif primarySpecies == f2.split(".")[0]:
            if primary==True:
                description = indexedF2[prot].description
            else:
                description = indexedF1[prot].description
        return (description)

    def getUnAssignedDomains(self, dommap, prot, numberedDomains):
        unAssignedDomains=[]
        for domainName in dommap[prot]:
            domName = "|".join(domainName.split("_"))
            if domName not in [x.split("_")[-1] for x in numberedDomains]:
                unAssignedDomains.append(domName)
        return(unAssignedDomains)

    def getAssignedDomains(self, domainClusters, sqltable, prot):
        numberedDomains=[]
        for g in domainClusters[prot]:
            for s in sqltable[g]:
                if s[0] == prot:
                    numberedDomains.append(g +"_"+"|".join(s[1:6]))
        return(numberedDomains)

    def getDiscordant(self):
        return(self.discordant)

class ResultMerger(object):
    def __init__(self, domainoid, inparanoid):
        self.merge = set()
        self.inparanoid = set()
        self.domainoid = set()
        self.conflicting = set()
        self.same = set()
        for domFile in os.listdir(domainoid):
            if domFile.endswith("-pairs"):
                for inFile in os.listdir(inparanoid):
                    name = domFile.replace(".dom", "")
                    revName = domFile.replace(".dom", "").split("-")[1]+"-"+domFile.replace(".dom", "").split("-")[0]+"-pairs"
                    if name == inFile or revName==inFile:
                        allInDoms = []
                        with open(inparanoid + inFile, 'r') as inTable:
                            for inOrth in inTable:
                                inA, inB = inOrth.rstrip().split()
                                pair = (self.getFixedPair(inA, inB))
                                self.merge.add(pair)
                                self.inparanoid.add(pair)
                                allInDoms.append(inA)
                                allInDoms.append(inB)

                        with open(domainoid + domFile, 'r') as domTable:
                            for domOrth in domTable:
                                domA, domB = domOrth.rstrip().split()
                                if (domA not in allInDoms) and (domB not in allInDoms):
                                    # new pair, adding
                                    pair = (self.getFixedPair(domA, domB))
                                    self.merge.add(pair)
                                    self.domainoid.add(pair)
                                elif (domA in allInDoms and domB not in allInDoms) or (domB in allInDoms and domA not in allInDoms) :
                                    # one new not paired with other, adding
                                    pair = (self.getFixedPair(domA, domB))
                                    self.merge.add(pair)
                                    self.domainoid.add(pair)
                                elif (domA in allInDoms) and (domB in allInDoms):
                                    pairV1= (self.getFixedPair(domA, domB))
                                    pairV2= (self.getFixedPair(domB, domA))
                                    if pairV1 in self.inparanoid or pairV2 in self.inparanoid:
                                        self.same.add(pairV1)
                                    else:
                                        # both exist, but are paired with others in inParanoid
                                        self.conflicting.add(pairV1)

    def getFixedPair(self, a, b):
        if "|" in a :
            fixedA = a.split("|")[len(a.split("|"))-1]
        else:
            fixedA = a
        if "|" in b :
            fixedB = b.split("|")[len(b.split("|"))-1]
        else:
            fixedB = b
        return (fixedA, fixedB)

    def getMerge(self):
        return(self.merge)
    def getInParanoid(self):
        return(self.inparanoid)
