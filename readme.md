# Domainoid
  
Domainoid is a tool for inference of domain based orthologs. It uses Pfam Scan to locate the domains in proteomes, and runs InParanoid on the domain sequences. This repository provides a script to run the pipeline and a singularity container definition containing all neccessary dependencies. If you wish to run the tool in a singularity container that downloads all dependencies needed, please follow the instructions under "Run using Docker container". Otherwise follow the instructions under the section "Requirements" to manually download all requirements and run the script.

The most recent version of Domainoid (v.1.1) is using InParanoid-DIAMOND. To use the previous, BLAST version of Domainoid, you can download the source code for it here [v.1.0](https://bitbucket.org/sonnhammergroup/domainoid/get/609cda180066d67867be04553f18d18d7049cd09.zip), or use the Docker container tagged with version: 1.0



[TOC]

# Run using Docker container

Domainoid is available as a docker container, including all dependencies needed to run the program. To use the docker container, you need to have docker installed on your local machine. 
See the [Docker website](https://docs.docker.com/engine/install/) for install instructions on different systems. The Domainoid container is managed and maintained on [Dockerhub](https://hub.docker.com/r/sonnhammer/domainoid) 

To download the container (note that you may have to use sudo for docker commands): 
	
	docker pull sonnhammer/domainoid
	
To run Domainoid in the container, you need to mount an input and two output directories to it using the -v command. This input-directory has to contain all files you want to analyze, and the program will automatically run all files in 
the directory. 
You can append any of the other run-options (see Input parameters below) to the end of the line. 
Note that text within <> must be replaced with absolute paths to your input and output directories. 

The following line will run version 1.1 of Domainoid with default settings on the input files located in the directory specified as <path/to/your/input/files>:
 
	docker run -v <path/to/input/directory/>:/home/domainoid/input/ -v <path/to/directory/for/output/>:/home/domainoid/output/ sonnhammer/domainoid

To use version 1.0 (with BLAST), use the following commands.

	docker pull sonnhammer/domainoid:v.1.0
	docker run -v <path/to/input/directory/>:/home/domainoid/input/ -v <path/to/directory/for/Domainoid/results/>:/home/domainoid/resultsDomainoid/ -v <path/to/directory/for/InParanoid/results/>:/home/domainoid/resultsInParanoid/ sonnhammer/domainoid:1.0

# Requirements
## Pfam 
  Pfam is required to identify the coordinates of the domains used for the inference of domain orthologs. Put these files in the same direcroty as the domainoid code, or provide location of your Pfam installation to the Domainoid script. Files required by the [PfamScan](http://ftp.ebi.ac.uk/pub/databases/Pfam/Tools/PfamScan.tar.gz) v01.03.2017

	# Data about Pfam domains
	wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam32.0/Pfam-A.hmm.gz
	gunzip -d Pfam-A.hmm.gz
	wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam32.0/Pfam-A.hmm.dat.gz
	gunzip -d Pfam-A.hmm.dat.gz
	wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/releases/Pfam32.0/active_site.dat.gz
	gunzip -d active_site.dat.gz
	# PfamScan tool
	wget ftp://ftp.ebi.ac.uk/pub/databases/Pfam/Tools/PfamScan.tar.gz
	tar xvfz PfamScan.tar.gz
   
PfamScan requires [BioPerl](http://bioperl.org/) v1.7.1 and [HMMER](http://hmmer.org/) v3.1b2

	# On GNU/Linux Debian 9
	apt-get install bioperl hmmer perl libmoose-perl libparallel-forkmanager-perl

### Prepare HMM database

	# Create *.h3* files required by PfamScan
	hmmpress Pfam-A.hmm

## DIAMOND
In addition to Pfam Scan dependencies, InParanoid algorithm requires [DIAMOD](https://github.com/bbuchfink/diamond). Put diamond in the inParanoid directory.

	wget http://github.com/bbuchfink/diamond/releases/download/v2.0.8/diamond-linux64.tar.gz
	tar xzf diamond-linux64.tar.gz

## Domainoid dependencies
Domainoid scripts are written in [Python3](https://www.python.org/downloads/release/python-353/) v3.5.3 and [BioPython](http://biopython.org/DIST/biopython-1.70.tar.gz) v1.70 is installed with [pip](https://pip.pypa.io/en/stable/)

	apt-get install python3 python3-pip 
	pip3 install biopython networkx sortedcontainers joblib
   
## Data
Data were obtained from [QfO](http://ftp.ebi.ac.uk/pub/databases/reference_proteomes/previous_releases/qfo_release-2018_04/QfO_release_2018_04.tar.gz) v2018.04 reference proteomes. Two small sample fasta files are provided in the directory of the code, and can be used for testing the tool. 

	wget ftp://ftp.ebi.ac.uk/pub/databases/reference_proteomes/previous_releases/qfo_release-2018_04/QfO_release_2018_04.tar.gz

## Run Domainoid   
In order to run Domainoid, use the runscript "runDomainoid.py" provided in the code. 

	# Run Domainoid
	python3 runDomainoid.py 
	-f EC SC   	# Fasta files used as input, separated by space, or a path to a directory containing fasta-files for running all vs all
	-lp './pfam' 	# Location of directory containing pfam
	-lps './pfam/pfamScan'  # Location of directory containing pfamScan

 
# Input Parameters 
In order to run Domainoid, use the runscript "runDomainoid.py". Pfam Scan will be used to extract domains from the input files. Proteins without detectable domains (using *default* PfamScan parameters) are included as a whole. After domain extraction residual part of sequence, should it be of sufficient length it is also returned as hypothetical domain.
The runtime of the tool vary with the size of the input. For the small sample files, results should be provided within a minute. For larger datasets, the runtime can be hours or days.

Running domainoid requires at leas two fasta-files as input. The location of these can be specified either as explicit paths to the files, separated by space, or as one path to the directory containing all your input fasta-files. In this case, Domainoid will try to read all files in the directory ending with .fa, .fasta or that does not have any file extension. When more than two files as provided as input, all will be run agains all, in parallel.

	-f EC SC # Fasta files used as input, separated by space, or a path to a directory containing all input files

Parameter -t is used to specify the alpha-threshold to use when recovering orthologs on a protein level. A threshold of 0.3 is used as default when no other is specified. This implies that all proteins having at least a third of their total domains in a common ortholog group is considered an orthologous pair on a protein level.

	-t 0.3 # Alpha threshold to use for recovery for orthologs on protein-level

Parameter -m is used to specify the minimum length of an un-annotated region to include as an unknown domain. A minimum length of 30 aa is used as default when nothing else is specified.

	-m 30 # Minimum length to include as a domain

Parameter -lp is used to specify the directory of pfam to use in the analysis.

	-lp './pfam' # Location of directory containing pfam

Parameter -lps is used to specify the directory of Pfam Scan to use in the analysis.

	-lps './pfam/pfamScan' # Location of directory containing pfamScan

Parameter -i can be used to also run regular InParanoid on the original input files. The results of this will be generated in the resultsInParanoid directory. When using this option, a file containing the merged set of protein pairs from Domainoid and InParanoid, will be generated in the resultsDomainoid directory. This set of pairs will contain all common protein pairs from both methods, all unique pairs from Domainoid an all unique pairs from InParanoid. If pairs are conflicting, InParanoid pairs will be prioritized. Please keep in mind that this optioin will double the runtime. 

	-i # Use this flag to also run regular inParanoid on the input files. Generates a file of merged protein orthologs from all inParanoid and Domainoid results in the resultsDomainoid-directory

Parameter -d can be used to generate a list of potential groups of Discordant proteins from Domainoid in the resultsDomainoid directory. The resulting file will contain Primary proteins and secondary proteins containing different subsets of the orthologous domains of the primary proteins.
 
	-d # Use flag to get a list of potentially discordant proteins in your resultsDomainoid directory

Parameter -s can be used to skip running of Pfam Scan. This option requires you to provide .tsv files with domain coordinates at the same location as your fasta files

	-s # Use this flag to skip running of Pfam Scan. This options requires you to provide .tsv files with domain coordinates at the same location as your fasta files

Parameter -p can be used to specify the number of processes to run in parallel. When nothing else is specified, 4 is used as a default.

	-p 4 # Number of processes to run in parallel

Option **-h** displays detailed help for each script parameter.
  
# Results
## Domainoid Result Files
Your result files will appear in this directory
The resulting files can be of different types, depending on the input parameters selected.

### Sqltable -file
This file is always generated, and contains all ortholog groups on a domain level, generated by InParanoind. The format of each line in the table will be:

	<group number>	<bitscore>	<species name>	<score>	<Protein name>||<sequence length>_<domain length>_<domain start coordinate>_<domain stop coordinate>_<domain name from pfam or UNKX>
	Example: 1	572	EC.dom	1.000	LEU2_ECOLI||465_451_6_456_PF00330.20

### DomainLevelPairs -file
This file is always generated, and contains a list of all pairs that can be generated on a domain level from the sqltable file, separated by tab. The format of the domain names will be:

	<Protein name>||<sequence length>_<domain length>_<domain start coordinate>_<domain stop coordinate>_<domain name from pfam or UNKX>
	Example: LEU2_ECOLI||465_451_6_456_PF00330.20	LEU2_YEAST||779_458_14_471_PF00330.20

### Pairs -file
This file is always generated, and contains a list of all pairs that can be recovered on a Protein level, based on the alpha threshold used as input. The list contains protein names, separated by tab.

	Example: LEU1_ECOLI	HOSC_YEAST

### Discordant -file
This file is generated when option -d is used as an input parameter. It contains a list of potential discordant proteins, grouped as primary proteins together with secondary proteins containing different subsets of the primary proteins orthologous domains. The format of the output will be:

	Primary: <Protein name and description from input fasta-file>
	<ortholog group>_<sequence length>|<domain length>|<domain start coordinate>|<domain stop coordinate>|<Domain name>
	Example: 
	Primary: UBIB_ECOLI
	9_546|314|233|546|UNK2, 47_546|117|116|232|PF03109.16, 546|115|1|115|UNK1
	Secondary: YP09_YEAST
	29_543|258|286|543|UNK2, 543|128|158|285|PF03109.16, 543|157|1|157|UNK1
	Secondary: YL53_YEAST
	47_569|123|166|288|PF03109.16, 569|165|1|165|UNK1, 569|281|289|569|UNK2

### MergedResults -file
This file will be generated when option -i is used as an input parameter. This flag will run Domainoid as well as regular InParanoid on the original input fasta-files. The resulting sets of ortholog pairs on a protein level from Domainoid and InParanoid will be merged, and when pair assignments are conflicting, the pairs from InParanoid will be kept. The resulting file will contain a list of protein names separated by tab. Please keep in mind that this will double the runtime.

	Example: AFUC_ECOLI	YFC8_YEAST

## InParanoid Result Files
When using input parameter -i, regular InParanoid will be run on the original input fasta-files, as well as Domainoid. The results from the regular InParanoid run will be stored in this directory.

### Sqltable -file
This file contains all ortholog groups generated by InParanoid from the input files. The format of each line in the table will be:

	<group number>	<bitscore>	<species name>	<score>	<Protein name>
	Example: 1	572	EC.dom	1.000	LEU2_ECOLI

### Pairs -file
This file contains a list of all protein pairs that can be generated from the sqltable file. The list contains protein names, separated by tab.
	
	Example: LEU1_ECOLI	HOSC_YEAST

