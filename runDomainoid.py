import os
import re
import argparse
from datetime import datetime
from sys import stdout
from Bio import SeqIO
from domainExtractor import DomainLoader
from domainExtractor import PseudoDomainLoader
from domainExtractor import DomainExporter
from joblib import Parallel, delayed
import multiprocessing
from domainoid import GroupTableReader
from domainoid import GroupTableReaderNormal
from domainoid import PotentialPairs
from domainoid import DomainClusters
from domainoid import OrthologPairs
from domainoid import CountDomains
from domainoid import ResultMerger
from domainoid import Discordant
import shutil
from itertools import combinations

parser = argparse.ArgumentParser(description='''
This script runs all steps of Domainoid. It takes either a number of fasta files as input, or a path to a directory containing all you input files.
If multiple fasta files are used as input, all are run against all. For each species pair, a file  is generated in the resultsDomainoid-directory
containing ortholog pairs on a protein level as well as on a domain level.
''', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument("-f", "--fasta",
                    help="Fasta files used as input separated by space, or a path to the directory containing input files",
                    nargs='+', required=True)
parser.add_argument("-o", "--output",
                    help="Fasta files used as input separated by space, or a path to the directory containing input files",
                    default="output"+str(datetime.now()).replace(" ","_"))
parser.add_argument("-i", "--inparanoid",
                    help="Also runs a regular InParanoid on the input files and merges the result with Domainoid results. Results are generated in the resultsInParanoid - directory",
                    default=False, action='store_true')
parser.add_argument("-s", "--skipPfam",
                    help="Skip running of Pfam. This options requires you to provide .tsv files with domain coordinates at the same location as your fasta files",
                    default=False, action='store_true')
parser.add_argument("-m", "--minlen",
                    help="Minimum length to include as domain",
                    type=int, default=30)
parser.add_argument("-t", "--threshold",
                    help="Threshold for domain orthologs inclusion.",
                    type=float, default=0.3)
parser.add_argument("-p", "--parallel",
                    help="Number of processes to run in parallel",
                    type=int, default=os.cpu_count())
parser.add_argument("-lp", "--locationPfam",
                    help="Location of directory containing pfam",
                    type=str, default="pfam/")
parser.add_argument("-lps", "--locationPfamScan",
                    help="Location of directory containing pfamScan",
                    type=str, default="pfam/PfamScan")
parser.add_argument("-d", "--discordant",
                    help="Use flag to get a list of discordant proteins in your resultsDomainoid directory",
                    default=False, action='store_true')
args = parser.parse_args()

def prepareFiles():
    domFiles=[]
    fastaFiles=[]
    filePath=""
    if len(args.fasta)<2:
        for entry in os.scandir(args.fasta[0]):
            if entry.is_file() and (entry.name.endswith(".fa") or entry.name.endswith(".fasta") or "." not in entry.name):
                fastaFiles.append(entry.name)
            elif entry.name!="readme.md":
                print("Cant use file "+entry.name+" since it is not recognized as an input fasta file. Please move or delete it from the input directory. ")
                exit()
        if args.fasta[0].endswith("/"):
            filePath += args.fasta[0]
        else:
            filePath += args.fasta[0]+"/"
        if filePath.startswith("./"):
            filePath=filePath[2:]
    else:
        for f in args.fasta:
            fastaFiles.append(f)
    for filename in fastaFiles:
        domFiles.append(filename+".dom")
    if os.path.isdir(args.output)==False:
        os.mkdir(args.output)
    if os.path.isdir(args.output+"/resultsDomainoid")==False:
        os.mkdir(args.output+"/resultsDomainoid")
    if args.inparanoid==True and os.path.isdir(args.output+"/resultsInParanoid")==False:
        os.mkdir(args.output+"/resultsInParanoid")
    if os.path.isdir(args.output+"/inputFiles_dom")==False:
        os.mkdir(args.output+"/inputFiles_dom")
    if os.path.isdir(args.output+"/inputFiles_tsv")==False:
        os.mkdir(args.output+"/inputFiles_tsv")
    return domFiles, fastaFiles, filePath


def createDomFiles(chunk, filePath):
    for filename in chunk:
        if args.skipPfam==False:
            print("Starting pfam on "+filePath+filename)
            os.system("perl -I "+args.locationPfamScan+" "+args.locationPfamScan+"/pfam_scan.pl -fasta "+filePath+filename+" -dir "+args.locationPfam+" -outfile "+args.output+"/inputFiles_tsv/"+filename+".tsv -cpu 1")
            print("pfam done, extracting domains")
        else:
            print("Skipping Pfam, working with existing tsv file")
        indexed = SeqIO.index(filePath+filename, format='fasta')
        domainloader = DomainLoader(args.output+"/inputFiles_tsv/"+filename+".tsv", indexed)
        repeats = domainloader.getRepeats()
        overlap = domainloader.getOverlap()
        contained = domainloader.getContained()
        collection = PseudoDomainLoader(
            domainloader.getNonOverlappingCollection(),
            minlen=args.minlen).getWithPseudoDomains()
        exporter = DomainExporter(collection, indexed, repeats, overlap, contained)
        exporter.exportTo(args.output+"/inputFiles_dom/"+filename+".dom")

def runInparanoid(inputDir, cores,outDirInparanoid):
    print("Starting InParanoid ")
    os. chdir("./inParanoid")
    if inputDir.startswith("/")==False:
        inputDir="../"+inputDir
    if outDirInparanoid.startswith("/")==False:
        outDirInparanoid="../"+outDirInparanoid
    os.system("perl inparanoid.pl -input-dir "+inputDir+" -out-dir "+outDirInparanoid+" -diamond-path ./diamond -seedscore -no-overwrite -cores "+str(cores))
    os. chdir("..")
    print("InParanoid done, sqltabe-file produced")

def extractDiscordant(file1, file2, filepath,outDirInparanoid,inputDirInparanoid):
    print("Extracting Discordant proteins to file: "+file1+"-"+file2+"-discordant")
    infile=outDirInparanoid+"/SQLtable."+file1+"-"+file2
    if os.path.isfile(infile)==False:
        infile=outDirInparanoid+"/SQLtable."+file2+"-"+file1
    reader = GroupTableReader(infile)
    domclu = DomainClusters(reader)
    fastaList=[]
    fastaList.append(inputDirInparanoid+"/"+file1)
    fastaList.append(inputDirInparanoid+"/"+file2)
    counter = CountDomains(fastaList)
    discordant = Discordant(reader, domclu, counter, file1, file2, filepath)
    with open(args.output+"/resultsDomainoid/"+file1+"-"+file2+"-discordant", 'w') as fout:
        fout.write("Potentially discordant proteins. Proteins with multiple orthologous domains have been selected as primary proteins, and the secondary protiens are added to the discordant list if they contain a subset of the domains of the Primary protein, and if not all of the secondary proteins contain the same subset. Secondary proteins contain at least one orthologous domain to the primary protein. The domains of the proteins are listed in the following format: <ortholog group>_<sequence length>|<domain length>|<domain start coordinate>|<domain stop coordinate>|<Domain name> ")
        for disc in discordant.getDiscordant():
            fout.write("\n".join(disc))

def extractProteinLevelOrthologPairs(file1, file2,outDirInparanoid):
    print("Extracting ortholog Protein pairs to file: "+file1+"-"+file2+"-pairs")
    infile=outDirInparanoid+"/SQLtable."+file1+"-"+file2
    if os.path.isfile(infile)==False:
        infile=outDirInparanoid+"/SQLtable."+file2+"-"+file1
    reader = GroupTableReader(infile)
    ppairs = PotentialPairs(reader)
    domclu = DomainClusters(reader)
    fastaList=[]
    fastaList.append(args.output+"/inputFiles_dom/"+file1)
    fastaList.append(args.output+"/inputFiles_dom/"+file2)
    counter = CountDomains(fastaList)
    opairs = OrthologPairs(ppairs, domclu, counter, threshold=args.threshold)
    with open(args.output+"/resultsDomainoid/"+file1+"-"+file2+"-pairs", 'w') as fout:
        for pair in opairs.getPairs():
            fout.write('\t'.join(list(pair)) + '\n')

def extractInParanoidPairs(file1, file2,outDirInparanoid):
    infile=outDirInparanoid+"/SQLtable."+file1+"-"+file2
    if os.path.isfile(infile)==False:
        infile=outDirInparanoid+"/SQLtable."+file2+"-"+file1
    reader = GroupTableReaderNormal(infile)
    with open(args.output+"/resultsInParanoid/"+file1+"-"+file2+"-pairs", 'w') as fout:
        for index, group in reader.readGroup().items():
            for A, B in combinations(group, 2):
                # If different species
                if A[-1] != B[-1]:
                    fout.write('\t'.join([A[0], B[0]]) + '\n')

def extractDomainLevelOrthologPairs(file1, file2,outDirInparanoid):
    print("Extracting ortholog Domain pairs to file: "+file1+"-"+file2+"-domainLevelPairs")
    infile=outDirInparanoid+"/SQLtable."+file1+"-"+file2
    if os.path.isfile(infile)==False:
        infile=outDirInparanoid+"/SQLtable."+file2+"-"+file1
    reader = GroupTableReader(infile)
    with open(args.output+"/resultsDomainoid/"+file1+"-"+file2+"-domainLevelPairs", 'w') as fout:
        for index, group in reader.readGroup().items():
            for A, B in combinations(group, 2):
                # If different species
                if A[-1] != B[-1]:
                    fout.write('\t'.join([A[0]+"||"+A[1]+"_"+A[2]+"_"+A[3]+"_"+A[4]+"_"+A[5], B[0]+"||"+B[1]+"_"+B[2]+"_"+B[3]+"_"+B[4]+"_"+B[5]]) + '\n')

def runAnalysis(chunk, runInParanoid, filepath):
    for f1,f2 in chunk:
        if runInParanoid == False:
            #If discordant flag is used, extract discordant list
            if args.discordant==True:
                extractDiscordant(f1, f2, filepath,args.output+"/resultsDomainoid/",args.output+"/inputFiles_dom")
            #Extracting domainoid domain-level pairs from sqltable file
            extractDomainLevelOrthologPairs(f1, f2,args.output+"/resultsDomainoid/")
            #Extracting domainoid protein-level pairs from sqltable file
            extractProteinLevelOrthologPairs(f1, f2,args.output+"/resultsDomainoid/")
            #Copies the sqltable file to the results directory, not to loose it
            # shutil.copy(os.getcwd()+"/inParanoid/output/SQLtable."+f1+"-"+f2, args.output+"/resultsDomainoid/SQLtable."+f1+"-"+f2)
        else:
            #Extracting regular inParanoid pairs from sqltable file
            extractInParanoidPairs(f1.replace(".dom",""), f2.replace(".dom",""),args.output+"/resultsInParanoid/")
            #Copies the sqltable file to the results directory, not to loose it
            # shutil.copy(os.getcwd()+"/inParanoid/output/SQLtable."+f1+"-"+f2, args.output+"/resultsInParanoid/SQLtable."+f1+"-"+f2)

def mergeResults():
    merger = ResultMerger(args.output+"/resultsDomainoid/", args.output+"/resultsInParanoid/")
    merge=merger.getMerge()
    print("Writing merged results to: "+args.output+"/resultsDomainoid/mergedResults")
    with open(args.output+"/resultsDomainoid/mergedResults", 'w') as fout:
        for pair in merge:
            fout.write('\t'.join(list(pair)) + '\n')

def chunkList(runList, parallel):
    if parallel>=len(runList):
        chunkSize=1
    else:
        chunkSize=int(len(runList)/parallel)
    chunkedRunlist=[]
    for i in range(0, len(runList), chunkSize):
        chunkedRunlist.append(runList[i:i+chunkSize])
    return chunkedRunlist

def makeRunlist(files, parallel,):
    runList=[]
    for f in os.listdir(args.output+"/resultsDomainoid/"):
        if f.startswith("SQLtable"):
            pair= (f.replace("SQLtable.","").split("-")[0], f.replace("SQLtable.","").split("-")[1])
            runList.append(pair)

    chunkedRunlist=chunkList(runList, parallel)
    return(chunkedRunlist)

###################################################################################################
# Everything is run from here. When multiple files are provided, all are run against all
###################################################################################################
print("\nRunning Domainoid v.1.1 with "+str(args.parallel)+" cores")
domFiles, fastaFiles, filePath = prepareFiles()

# Check for results that are already done:
numInputFiles=0
for file in os.listdir(filePath):
    if file!="readme.md":
        numInputFiles+=1
print("Number of input files: "+str(numInputFiles))
doneDomainoid=[]
DomainoidDone=False
if os.path.isdir(args.output+"/resultsDomainoid/")==True:
    for file in os.listdir(args.output+"/resultsDomainoid/"):
        if file.startswith("SQLtable"):
            doneDomainoid.append(file.replace("SQLtable.",""))
if len(doneDomainoid)==(numInputFiles*(numInputFiles-1))/2:
    DomainoidDone=True
    print("Skipping Domainoid, already done")
doneInParanoid=[]
InParanoidDone=False
if os.path.isdir(args.output+"/resultsInParanoid/")==True:
    for file in os.listdir(args.output+"/resultsInParanoid/"):
        if file.startswith("SQLtable"):
            doneInParanoid.append(file.replace("SQLtable.",""))
print("Done domainoid files: "+ str(len(doneDomainoid)))
if len(doneInParanoid)==(numInputFiles*(numInputFiles-1))/2:
    InParanoidDone=True
    print("Skipping InParanoid, already done")

#Creating fasta.dom files using Pfam
chunkedFilesList=chunkList(fastaFiles, args.parallel)
if DomainoidDone==False:
    Parallel(n_jobs=args.parallel)(delayed(createDomFiles)(chunk, filePath) for chunk in chunkedFilesList)

#Makes a list of all jobs to run (for all vs all) and runs inParanoid in parallel
if DomainoidDone==False:
    runInparanoid(args.output+"/inputFiles_dom", args.parallel,args.output+"/resultsDomainoid/")
    runList=makeRunlist(domFiles, args.parallel)
    Parallel(n_jobs=args.parallel)(delayed(runAnalysis)(chunk, False, filePath) for chunk in runList)
    # shutil.rmtree(os.getcwd()+"/inParanoid/output")
#When all domainoid runs are done, inParanoid is being run on the original fasta files (if this option is selected)
if args.inparanoid==True:
    if InParanoidDone==False:
        runInparanoid(filePath, args.parallel,args.output+"/resultsInParanoid/")
        runList=makeRunlist(fastaFiles, args.parallel)
        Parallel(n_jobs=args.parallel)(delayed(runAnalysis)(chunk, True, filePath) for chunk in runList)
        # shutil.rmtree(os.getcwd()+"/inParanoid/output")
    #When all results are done, they are merged and written to the mergedResults file
    mergeResults()
